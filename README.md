# Theme de base
https://github.com/getgrav/grav-theme-saturn <3

# git ignore
- Ne pas exclure les themes
```!user/themes/*```
- Mais exclure
```
tests/*
system/src/*
system/images/*
system/languages/*
```