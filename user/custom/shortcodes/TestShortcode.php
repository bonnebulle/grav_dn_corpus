<?php
namespace Grav\Plugin\Shortcodes;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;
class TestShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('test', function(ShortcodeInterface $sc) {
            return '<test>'.$sc->getContent().'</test>';
        });
    }
}