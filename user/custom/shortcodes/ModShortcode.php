<?php
namespace Grav\Plugin\Shortcodes;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;
class ModShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('mod', function(ShortcodeInterface $sc) {
            $titre = $sc->getParameter('titre', false);
            $url = $sc->getParameter('url', false);
            if($titre && $url) {
                return '<mod data-titre="'.$titre.'" data-url="'.$url.'"><span class="mod_titre url"><a href="'.$url.'">'.$titre.'</a></span><span class="mod_content">'.$sc->getContent().'</span></mod>';
           }
           else if($titre) {
                 return '<mod data-titre="'.$titre.'"><span class="mod_titre">'.$titre.'</span><span class="mod_content">'.$sc->getContent().'</span></mod>';
            }
            else {
                return '<mod><span class="mod_content">'.$sc->getContent().'</span></mod>';
            }
        });
    }
}